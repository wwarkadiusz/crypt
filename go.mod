module github.com/bketelsen/crypt

go 1.12

require (
	cloud.google.com/go/firestore v1.1.0
	filippo.io/age v1.0.0
	github.com/armon/go-metrics v0.3.9 // indirect
	github.com/fatih/color v1.12.0 // indirect
	github.com/go-redis/redis/v9 v9.0.0-beta.2
	github.com/hashicorp/consul/api v1.11.0
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-hclog v0.16.2 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	go.etcd.io/etcd/client/v2 v2.305.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	google.golang.org/api v0.44.0
	google.golang.org/grpc v1.41.0
)
