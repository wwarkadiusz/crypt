package redis

import (
	"context"
	"errors"
	"fmt"
	"github.com/bketelsen/crypt/backend"
	"github.com/go-redis/redis/v9"
)

type Client struct {
	redisClient *redis.Client
}

func New(address, password string, db int) (*Client, error) {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       db,
	})

	return &Client{redisClient}, nil
}

func (c *Client) Get(key string) ([]byte, error) {
	val, err := c.redisClient.Get(context.Background(), key).Result()

	switch {
	case err == redis.Nil:
		return nil, fmt.Errorf("key \"%s\" not found", key)
	case err != nil:
		return nil, fmt.Errorf("could not get the value from Redis, error: %w", err)
	}

	return []byte(val), nil
}

// Set sets the provided key to value.
func (c *Client) Set(key string, value []byte) error {
	return c.redisClient.Set(context.Background(), key, string(value), 0).Err()
}

// List retrieves all keys and values under a provided key.
func (c *Client) List(key string) (backend.KVPairs, error) {
	return nil, errors.New("not supported")
}

// Watch monitors a K/V store for changes to key.
func (c *Client) Watch(key string, stop chan bool) <-chan *backend.Response {
	panic("not supported")
}
